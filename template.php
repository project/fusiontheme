<?php

function fusiontheme_preprocess_page(&$vars) {
  $vars['footer_msg'] = ' &copy; ' . $vars['site_name'] . ' ' . date('Y');
  $vars['search_box'] = str_replace(t('Search this site: '), '', $vars['search_box']);
  
  $vars['p_links'] = '';
  if(!empty($vars['primary_links'])) {
    $vars['p_links'] = '<ul>';
    foreach ($vars['primary_links'] as $link) {
      $link_current = '';
      $href = url($link['href']);
      if ($link['href'] == '<front>' && drupal_is_front_page()) {
        $link_current = ' current_page_item';
      }

      if($link['href'] == $_GET['q']) {
        $link_current = ' current_page_item';
      }
       
      $vars['p_links'] .= '<li class="pageitem' . $link_current . '"><a href="' . $href . '"><span><span>' . $link['title'] . '</span></span></a></li>';
    }
    $vars['p_links'] .= '</ul>';
  }
  
}
  
function fusiontheme_preprocess_node(&$vars) {
  $vars['postdate'] = format_date($vars['node']->created, 'custom', 'd F Y');
  $vars['author'] = theme('username', $vars['node']);
  $vars['posted_by'] = t('By') . ' ' . $vars['author'] . ' ' . t('on') . ' ' . $vars['postdate'];
}

function fusiontheme_preprocess_comment_wrapper(&$vars) {
  $node = $vars['node'];
  $vars['header'] = t('<strong>!count comments</strong> on %title', array('!count' => $node->comment_count, '%title' => $node->title));
}

function fusiontheme_preprocess_comment(&$vars) {
  $vars['classes'] = array('comment');
  $vars['classes'] = implode(' ', $vars['classes']);
}


function fusiontheme_search_theme_form($form) {
  $form['submit']['#type'] = 'button';
  $form['submit']['#attributes']['class'] = 'searchbutton';
  $form['submit']['#value'] = t('Go');
  $form['search_theme_form']['#attributes']['class'] = 'searchfield';
  $form['search_theme_form']['#title'] = t('');
  return drupal_render($form);
}
