<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lte IE 6]>
<script type="text/javascript">
/* <![CDATA[ */
   blankimgpath = '<?php print base_path() . path_to_theme() ?>/images/blank.gif';
 /* ]]> */
</script>
<style type="text/css" media="screen">
  @import "<?php print base_path() . path_to_theme() ?>/ie6.css";
  body{ behavior:url("<?php print base_path() . path_to_theme() ?>/js/ie6hoverfix.htc"); }
  img{ behavior: url("<?php print base_path() . path_to_theme() ?>/js/ie6pngfix.htc"); }
</style>
<![endif]-->

</head>
  <body class="home">
  <!-- page wrappers (100% width) -->
  <div id="page-wrap1">
    <div id="page-wrap2">
      <!-- page (actual site content, custom width) -->
      <div id="page" class="with-sidebar">

       <!-- main wrapper (side & main) -->
       <div id="main-wrap">
        <!-- mid column wrap -->
    	<div id="mid-wrap">
          <!-- sidebar wrap -->
          <div id="side-wrap">
            <!-- mid column -->
    	    <div id="mid">
              <!-- header -->
              <div id="header">
                <div id="topnav" class="description"><?php print $site_slogan; ?></div>
                  <h1 id="title"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>


                <!-- top tab navigation -->
                <div id="tabs">
             		<?php print $p_links ?>
                </div>
                <!-- /top tabs -->

              </div><!-- /header -->
              <!-- mid content -->
			  <div id="mid-content">
 			    <?php if (!empty($breadcrumb)): ?><div class="drupal-breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
   			    <?php if (!empty($title) && !$node): ?><h3 class="title"><?php print $title; ?></h3><?php endif; ?>
                <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
   				<?php if (!empty($messages)): print $messages; endif; ?>
                <?php if (!empty($help)): print $help; endif; ?>
                <?php if ($is_front && !empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
    
				<?php print $content; ?>
			  </div>
			  
<!-- mid content -->
</div>
<!-- /mid -->

<!-- sidebar -->
<div id="sidebar">
 <!-- sidebar 1st container -->
 <div id="sidebar-wrap1">
  <!-- sidebar 2nd container -->
  <div id="sidebar-wrap2">
     <ul id="sidelist">

        <!-- wp search form -->
        <li>
         <div class="widget">
         <?php if ($search_box): ?>
          <div id="searchtab">
            <div class="inside">
            <?php print $search_box ?>        
            </div>
          </div>
          <?php endif; ?>
         </div>
        </li>
        <!-- / search form -->
		
     <?php print $right; ?>
     </ul>
  </div>
  <!-- /sidebar 2nd container -->
 </div>
 <!-- /sidebar 1st container -->
</div>
<!-- /sidebar -->


  </div>
  <!-- /side wrap -->
 </div>
 <!-- /mid column wrap -->
</div>
<!-- /main wrapper -->

<!-- clear main & sidebar sections -->
<div class="clearcontent"></div>
<!-- /clear main & sidebar sections -->

 <div class="clear"></div>

<!-- footer -->
 <div id="footer">


   <p>
    <?php print $footer_msg; ?> <?php print $footer_message; ?><br /><a href="http://digitalnature.ro/projects/fusion">digitalnature</a> fusion theme ported by <a href="http://topdrupalthemes.net">Drupal Themes</a> | <a href="http://phpweby.com/hostgator_coupon.php">hostgator coupon code</a>
    | <?php print $feed_icons ?>

   </p>
 </div>
 <!-- /footer -->

</div>
<!-- /page -->

</div>
</div>
<!-- /page wrappers -->


<?php print $closure; ?>
</body>
</html>
