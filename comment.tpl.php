<?php

/**
 * @file comment.tpl.php
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: Body of the post.
 * - $date: Date and time of posting.
 * - $links: Various operational links.
 * - $new: New comment marker.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $submitted: By line with date and time.
 * - $title: Linked title.
 *
 * These two variables are provided for context.
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * @see template_preprocess_comment()
 * @see theme_comment()
 */
?>
<li class="<?php print $classes ?><?php if ($picture): ?> with-avatars<?php endif; ?>">
  <div class="wrap tiptrigger">
    <?php print $picture ?>
    <div class="details regularcomment">
      <p class="head">
        <span class="info">
          <?php print t('By ') . $author . t(' on ') ?><?php print $date ?>
        </span>
      </p>
      <!-- comment contents -->
      <div class="text">
		<div>
          <?php if ($status == 'comment-unpublished') : ?>
            <p class="moderate"><?php print t('Your comment is awaiting moderation.'); ?></p>
          <?php endif; ?>
          <?php print $content ?>
          <?php if ($signature): ?>
	        <div class="user-signature clear-block">
              <?php print $signature ?>
            </div>
          <?php endif; ?>
          <?php print $links ?>   
        </div>
      </div>
       <!-- /comment contents -->
    </div>
  </div>
</li>
