<?php
?><li id="block-<?php print $block->module .'-'. $block->delta; ?>" class="box-wrap block-<?php print $block->module ?>" >
    <div class="box">
      <?php if ($block->subject): ?>
        <h2 class="title"><?php print $block->subject ?></h2>
      <?php endif;?>
      <div class="inside">
        <?php print $block->content ?>
      </div>
    </div>
  </li>